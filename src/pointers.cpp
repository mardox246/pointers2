#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;


 

 cout<<"the value in x is = "<<x<<" and the address is "<<&x<<endl;
 cout<<"the value in y is = "<<y<<" and the address is "<<&y<<endl;
 cout<<"the value in a is = "<<a<<" and the address is "<<&a<<endl;
 cout<<"the value in b is = "<<b<<" and the address is "<<&b<<endl;
 cout<<"the value in c is = "<<c<<" and the address is "<<&c<<endl;
 cout<<"the value in d is = "<<d<<" and the address is "<<&d<<endl;
 



}
